/** Отображение города в список покупателей, проживающих в этом городе. */
fun Shop.groupCustomersByCity(): Map<City, List<Customer>> = customers.groupBy { customer -> customer.city }

/**
 * Отображение каждого продукта в множество покупателей, хотя бы раз заказавших этот продукт.
 * Продукты, которые никто никогда не купил, не должны присутствовать в результате.
 */
fun Shop.groupCustomersByOrderedProduct(): Map<Product, Set<Customer>> =
    getAllOrderedProducts().associateBy({ product -> product }, { product ->
        buildSet {
            customers.forEach { customer ->
                if (product in customer.getOrderedProducts()) add(customer)
            }
        }
    }).filter { it.value.isNotEmpty() }