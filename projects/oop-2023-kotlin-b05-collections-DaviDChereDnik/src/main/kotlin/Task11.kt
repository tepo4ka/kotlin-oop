/** Самый дорогой продукт из всех когда-либо доставленных ([Order.isDelivered]) */
fun Customer.getMostExpensiveDeliveredProduct(): Product? =
    orders.filter { order -> order.isDelivered }.flatMap { order -> order.products }
        .maxByOrNull { product -> product.price }

/** Сколько раз продукт был заказан? (учтите, что один покупатель может заказать продукт несколько раз) */
fun Shop.getNumberOfTimesProductWasOrdered(product: Product): Int =
    customers.flatMap { customer -> customer.orders }.flatMap { order -> order.products }
        .count { curProduct -> curProduct == product }