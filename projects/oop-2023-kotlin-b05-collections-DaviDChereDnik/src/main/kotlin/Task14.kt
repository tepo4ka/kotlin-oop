import java.io.File

/**
 * Напишите программу, считывающую текстовый файл, путь к которому указан в первом аргументе,
 * и выводящую все строки, в которых сумма целых чисел равна нулю.
 *
 * Целые числа в строке могут быть разделены табуляциями или пробелами, в том числе несколькими.
 *
 * Строки, начинающиеся с символа `#` нужно игнорировать.
 * Строки с некорректными данными нужно игнорировать.
 */
fun main(args: Array<String>) {
    File(args.first()).readLines().forEach { line ->
        if (line.isEmpty() || line.first() == '#') return@forEach

        val curNumbers = line.split("\\s+".toRegex()).map { it.toIntOrNull() ?: return@forEach }
        if (curNumbers.isNotEmpty() && curNumbers.sum() == 0) println(line)
    }
}