fun doSomethingStrangeWithCollection(collection: Collection<String>): Collection<String>? {
    val groupsByLength = collection.groupBy { it.length }
    val maximumSizeOfGroup = groupsByLength.values.maxOf { group -> group.size }
    return groupsByLength.values.firstOrNull { group -> group.size == maximumSizeOfGroup }
}