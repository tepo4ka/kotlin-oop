/** Продукты, которые хоть раз заказали все покупатели. */
fun Shop.getSetOfProductsOrderedByEveryCustomer(): Set<Product> =
    customers.map { customer -> customer.getOrderedProducts() }
        .reduce { result, customer -> result.intersect(customer) }