private inline fun <E> Iterable<E>.indexesOf(predicate: (E) -> Boolean) =
    mapIndexedNotNull { index, elem -> index.takeIf { predicate(elem) } }

private fun <E> Iterable<E>.indexesOf(element: E) = indexesOf { it == element }

class OddList<T>(private val originalList: List<T>) : List<T> {
    private val isOdd = { x: Int -> x % 2 != 0 }

    override val size: Int
        get() = originalList.size / 2  // division is floored

    override fun contains(element: T): Boolean = indexOf(element) != -1

    override fun containsAll(elements: Collection<T>): Boolean = elements.all { element -> contains(element) }

    override fun get(index: Int): T = originalList[2 * index + 1]

    override fun indexOf(element: T): Int = (originalList.indexesOf(element).firstOrNull(isOdd) ?: -2) / 2

    override fun isEmpty(): Boolean = originalList.size < 2  // if there's just 1 or no elements

    override fun iterator(): Iterator<T> = listIterator()

    override fun lastIndexOf(element: T): Int =
        (originalList.indexesOf(element).lastOrNull(isOdd) ?: -2) / 2

    override fun listIterator(): ListIterator<T> = listIterator(0)

    override fun listIterator(index: Int): ListIterator<T> = object : ListIterator<T> {
        private val originalIterator = originalList.listIterator(2 * index + 1)

        override fun hasNext(): Boolean = when {
            !originalIterator.hasNext() -> false
            !isOdd(originalIterator.nextIndex()) -> {
                originalIterator.next()
                originalIterator.hasNext()
            }

            else -> true
        }

        override fun hasPrevious(): Boolean = when {
            !originalIterator.hasPrevious() -> false
            !isOdd(originalIterator.previousIndex()) -> {
                originalIterator.previous()
                originalIterator.hasPrevious()
            }

            else -> true
        }

        override fun next(): T = when {
            !hasNext() -> throw NoSuchElementException()
            else -> originalIterator.next()
        }

        override fun previous(): T = when {
            !hasPrevious() -> throw NoSuchElementException()
            else -> originalIterator.previous()
        }

        override fun nextIndex(): Int = when {
            !hasNext() -> size
            else -> originalIterator.nextIndex() / 2
        }

        override fun previousIndex(): Int = when {
            !hasPrevious() -> -1
            else -> originalIterator.previousIndex() / 2
        }
    }

    override fun subList(fromIndex: Int, toIndex: Int): List<T> = object : List<T> {
        override val size: Int
            get() = toIndex - fromIndex

        override fun get(index: Int): T = this@OddList[fromIndex + index]

        override fun isEmpty(): Boolean = size == 0

        override fun iterator(): Iterator<T> = listIterator()

        override fun listIterator(): ListIterator<T> = listIterator(0)

        override fun listIterator(index: Int): ListIterator<T> = object : ListIterator<T> {
            private val parentIterator = this@OddList.listIterator(fromIndex)

            override fun hasNext(): Boolean = parentIterator.hasNext() && parentIterator.nextIndex() < toIndex

            override fun hasPrevious(): Boolean =
                parentIterator.hasPrevious() && parentIterator.previousIndex() >= fromIndex

            override fun next(): T = when {
                !hasNext() -> throw NoSuchElementException()
                else -> parentIterator.next()
            }

            override fun previous(): T = when {
                !hasPrevious() -> throw NoSuchElementException()
                else -> parentIterator.previous()
            }

            override fun nextIndex(): Int = when {
                !hasNext() -> size
                else -> parentIterator.nextIndex()
            }

            override fun previousIndex(): Int = when {
                !hasPrevious() -> -1
                else -> parentIterator.previousIndex()
            }
        }

        override fun subList(newFromIndex: Int, newToIndex: Int): List<T> =
            this@OddList.subList(fromIndex + newFromIndex, toIndex + newToIndex)

        override fun lastIndexOf(element: T): Int = this@OddList.indexesOf(element).lastOrNull { it < toIndex } ?: -1

        override fun indexOf(element: T): Int = this@OddList.indexesOf(element).firstOrNull { it < toIndex } ?: -1

        override fun containsAll(elements: Collection<T>): Boolean = elements.all { element -> contains(element) }

        override fun contains(element: T): Boolean = indexOf(element) != -1
    }

    override fun equals(other: Any?): Boolean = when {
        other === this -> true
        other !is List<*> -> false
        size != other.size -> false
        else -> {
            val thisIterator = listIterator()
            val otherIterator = other.listIterator()
            var result = true

            while (thisIterator.hasNext() && otherIterator.hasNext())
                if (thisIterator.next() != otherIterator.next()) {
                    result = false
                    break
                }

            result
        }
    }

    override fun hashCode(): Int {
        var hash = 1

        for (e in this)
            hash = 31 * hash + (e?.hashCode() ?: 0)

        return hash
    }
}

fun main() {
    val originalList = mutableListOf(0, 1, 2, 3, 4, 5)
    val oddList = OddList(originalList)
    println(oddList.joinToString()) // 1, 3, 5
    originalList[3] = -3
    println(oddList.joinToString()) // 1, -3, 5
}