/** Покупатели, у которых больше недоставленных заказов, чем доставленных - [Order.isDelivered] */
fun Shop.getCustomersWithMoreUndeliveredOrdersThanDelivered(): Set<Customer> =
    customers.partition { customer -> customer.orders.count { order -> order.isDelivered } < customer.orders.size }.first.toSet()

/** Список покупателей в таком порядке: сначала сделавшие хотя бы два заказа, затем все остальные
 * (внутри частей порядок должен совпадать с порядком покупателей в оригинальном списке [Shop.customers]) */
fun Shop.reorderCustomersAtLeastTwoOrdersFirst(): List<Customer> =
    customers.partition { customer -> customer.orders.size >= 2 }.toList().flatten()