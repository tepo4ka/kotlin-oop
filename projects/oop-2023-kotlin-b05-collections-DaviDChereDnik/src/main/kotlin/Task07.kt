/** Список покупателей по возрастанию количества заказов */
fun Shop.getCustomersSortedByNumberOfOrders(): List<Customer> = customers.sortedBy { customer -> customer.orders.size }

/** Сортировка покупателей по городу (в алфавитном порядке), а в пределах города –
 * по убыванию общей стоимости всех заказов */
fun Shop.customersReportByCity(): List<Customer> = customers.sortedWith { c1, c2 ->
    val c1Price = c1.getTotalOrderPrice()
    val c2Price = c2.getTotalOrderPrice()
    when {
        c1.city.name < c2.city.name || (c1.city.name == c2.city.name && c1Price > c2Price) -> -1
        c1.city.name > c2.city.name || (c1.city.name == c2.city.name && c1Price < c2Price) -> 1
        else -> 0
    }
}