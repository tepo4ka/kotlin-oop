/** Покупатель с самым большим числом заказов */
fun Shop.getCustomerWithMaximumNumberOfOrders(): Customer? = customers.maxByOrNull { customer -> customer.orders.size }

/** Самый дорогой продукт из всех когда-либо заказанных данным покупателем */
fun Customer.getMostExpensiveOrderedProduct(): Product? = getOrderedProducts().maxByOrNull { order -> order.price }