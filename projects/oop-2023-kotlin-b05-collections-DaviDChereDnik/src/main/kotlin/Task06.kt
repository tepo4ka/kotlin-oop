// Вычислите общую стоимость заказанных покупателем товаров.
// Замечание: покупатель может заказывать один и тот же товар несколько раз.
fun Customer.getTotalOrderPrice(): Double =
    orders.flatMap { order -> order.products }.sumOf { product -> product.price }