import io.github.oshai.kotlinlogging.KotlinLogging

private val logger = KotlinLogging.logger {}

class Field<E>(
    val height: Int, val width: Int, var fieldAccessor: FieldAccessor<E>, private val defaultValue: () -> E,
    init: (row: Int, col: Int) -> E = { _, _ -> defaultValue() }
) : List<List<E>> by List(height, { row -> List(width, { col -> init(row, col) }) }) {
    init {
        require(width > 0 && height > 0) { logger.error { "Can't initialize field with no columns and/or rows" } }
        logger.trace { "Created Field ${width}x${height} with default value ${defaultValue()}" }
    }

    operator fun get(row: Int, col: Int): E = fieldAccessor.get(row, col, defaultValue, this)
}

fun <E> List<List<E>>.forEachCellIndexed(action: (rowN: Int, colN: Int, cell: E) -> Unit): Unit =
    this.forEachIndexed { rowN, row -> row.forEachIndexed { colN, cell -> action(rowN, colN, cell) } }

fun <E> List<List<E>>.forEachCell(action: (cell: E) -> Unit): Unit =
    this.forEachCellIndexed { _, _, cell -> action(cell) }