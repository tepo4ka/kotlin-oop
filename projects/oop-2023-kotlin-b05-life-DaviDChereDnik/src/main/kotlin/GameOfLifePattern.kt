// Collection of frequently occurring patterns
// (more accurately, field regions that generate this patterns in the clean environment when used with classical rules)

data class GameOfLifePattern(val region: List<List<Int>>, val type: GameOfLifePatternType, val period: Int = 0)

enum class GameOfLifePatternType {
    Still, Oscillator, Spaceship
}

fun GameOfLife.placePattern(pattern: GameOfLifePattern, fromRow: Int, fromCol: Int) =
    pattern.region.forEachCellIndexed { rowN, colN, cell ->
        this.field[fromRow + rowN, fromCol + colN].alive = cell == 1
    }

enum class GameOfLifeDefaultPatterns(val pattern: GameOfLifePattern) {
    Block(
        // https://conwaylife.com/wiki/Block
        GameOfLifePattern(
            listOf(
                listOf(1, 1),
                listOf(1, 1)
            ), GameOfLifePatternType.Still
        )
    ),

    Beehive(
        // https://conwaylife.com/wiki/Beehive
        GameOfLifePattern(
            listOf(
                listOf(0, 1, 1, 0),
                listOf(1, 0, 0, 1),
                listOf(0, 1, 1, 0)
            ), GameOfLifePatternType.Still
        )
    ),

    Loaf(
        // https://conwaylife.com/wiki/Loaf
        GameOfLifePattern(
            listOf(
                listOf(0, 1, 1, 0),
                listOf(1, 0, 0, 1),
                listOf(0, 1, 0, 1),
                listOf(0, 0, 1, 0)
            ), GameOfLifePatternType.Still
        )
    ),

    Tub(
        // https://conwaylife.com/wiki/Tub
        GameOfLifePattern(
            listOf(
                listOf(0, 1, 0),
                listOf(1, 0, 1),
                listOf(0, 1, 0)
            ), GameOfLifePatternType.Still
        )
    ),


    Blinker(
        // https://conwaylife.com/wiki/Blinker
        GameOfLifePattern(
            listOf(
                listOf(0, 0, 0),
                listOf(1, 1, 1),
                listOf(0, 0, 0)
            ), GameOfLifePatternType.Oscillator, 2
        )
    ),

    Beacon(
        // https://conwaylife.com/wiki/Beacon
        GameOfLifePattern(
            listOf(
                listOf(1, 1, 0, 0),
                listOf(1, 1, 0, 0),
                listOf(0, 0, 1, 1),
                listOf(0, 0, 1, 1)
            ), GameOfLifePatternType.Oscillator, 2
        )
    ),

    Pulsar(
        // https://conwaylife.com/wiki/Pulsar
        GameOfLifePattern(
            listOf(
                listOf(0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0),
                listOf(0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0),
                listOf(0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0),
                listOf(0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0),
                listOf(0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0),
                listOf(1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1),
                listOf(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                listOf(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                listOf(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                listOf(1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1),
                listOf(0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0),
                listOf(0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0),
                listOf(0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0),
                listOf(0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0),
                listOf(0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0),
            ), GameOfLifePatternType.Oscillator, 3
        )
    ),

    Glider(
        // https://conwaylife.com/wiki/Glider
        GameOfLifePattern(
            listOf(
                listOf(1, 0, 0),
                listOf(0, 1, 1),
                listOf(1, 1, 0)
            ), GameOfLifePatternType.Spaceship, 4
        )
    )
}