import kotlin.random.Random

enum class GameOfLifeSeed(val seed: (row: Int, col: Int) -> Cell, val description: String) {
    `Kind random`(
        { _, _ -> Cell(alive = Random.nextBoolean()) },
        "Randomly set if cell is alive, but don't modify its generations count"
    ),

    `Evil random`({ _, _ ->
        Cell(
            alive = Random.nextBoolean(),
            generations = 0
        )
    }, "Randomly set if cell is alive and zero its generations count"),

    Clear({ _, _ -> Cell() }, "Fully clear the field")
}