import androidx.compose.foundation.layout.Box
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue

@Composable
fun GameOfLifeFieldReseed(state: GameOfLifeViewModel) {
    var isExpanded by remember { mutableStateOf(false) }

    Box {
        PlainTooltip("Regenerate current field using one of provided generators") {
            IconButton(onClick = { isExpanded = !isExpanded }) {
                Icon(Icons.Filled.Refresh, "")
            }
        }

        DropdownMenu(expanded = isExpanded, onDismissRequest = { isExpanded = false }) {
            GameOfLifeSeed.values().forEach { seed ->
                PlainTooltip(seed.description) {
                    DropdownMenuItem(onClick = {
                        state.game.reseed(seed)

                        isExpanded = false
                    }) {
                        Text("$seed")
                    }
                }
            }
        }
    }
}