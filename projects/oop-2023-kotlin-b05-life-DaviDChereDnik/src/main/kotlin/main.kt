import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.key.Key
import androidx.compose.ui.input.key.KeyEventType
import androidx.compose.ui.input.key.key
import androidx.compose.ui.input.key.type
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.delay

@OptIn(ExperimentalComposeUiApi::class)
fun main() = application {
    val logger = KotlinLogging.logger {}

    logger.info { "Starting GOF" }

    val gameState = GameOfLifeViewModel(
        GameOfLife(
            fieldHeight = 128,
            fieldWidth = 128,
            fieldSeed = GameOfLifeSeed.`Evil random`.seed
        )
    )

    Window(
        title = "GameOfLife",
        onCloseRequest = {
            logger.info { "Exiting GOF" }
            exitApplication()
        },
        onKeyEvent = {
            if (it.type == KeyEventType.KeyDown)
                when (it.key) {
                    Key.Spacebar -> {
                        if (!gameState.proceedGenerate)
                            gameState.game.nextGeneration()

                        true
                    }

                    Key.Enter -> {
                        if (!gameState.proceedGenerate)
                            gameState.infiniteGenerate = !gameState.infiniteGenerate

                        true
                    }

                    Key.Equals -> {
                        gameState.scale(1)

                        true
                    }

                    Key.Minus -> {
                        gameState.scale(-1)

                        true
                    }

                    Key.DirectionLeft, Key.H -> {
                        gameState.offset(
                            PanDirection.Left.offset * gameState.panOffset
                        )
                        true
                    }

                    Key.DirectionRight, Key.L -> {
                        gameState.offset(
                            PanDirection.Right.offset * gameState.panOffset
                        )
                        true
                    }

                    Key.DirectionUp, Key.K -> {
                        gameState.offset(
                            PanDirection.Up.offset * gameState.panOffset
                        )
                        true
                    }

                    Key.DirectionDown, Key.J -> {
                        gameState.offset(
                            PanDirection.Down.offset * gameState.panOffset
                        )
                        true
                    }

                    Key.C -> {
                        gameState.resetGeometry()

                        true
                    }

                    else -> false
                } else false
        }) {
        LaunchedEffect(gameState.infiniteGenerate) {
            while (gameState.infiniteGenerate) {
                gameState.game.nextGeneration()
                delay(gameState.generateDelay)
            }
        }

        LaunchedEffect(gameState.proceedGenerate) {
            while (gameState.proceedGenerate && gameState.generateAmount > 0) {
                gameState.game.nextGeneration()
                gameState.generateAmount--

                delay(gameState.generateDelay)
            }

            if (gameState.generateAmount == 0)
                gameState.proceedGenerate = false
        }

        Column(modifier = Modifier.fillMaxSize()) {
            Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
                GameOfLifeFiniteGenerate(gameState)
                GameOfLifeInfiniteGenerate(gameState)

                GameOfLifeFieldReseed(gameState)

                GameOfLifeSettings(gameState)
            }
            GameOfLifeFieldView(gameState)
        }
    }
}