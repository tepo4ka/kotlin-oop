import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.TooltipArea
import androidx.compose.foundation.TooltipPlacement
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.unit.dp

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun PlainTooltip(message: String, content: @Composable () -> Unit) = TooltipArea(
    tooltip = {
        Surface(
            modifier = Modifier.shadow(4.dp),
            color = MaterialTheme.colors.secondary,
            shape = RoundedCornerShape(4.dp)
        ) {
            Text(
                text = message,
                modifier = Modifier.padding(10.dp)
            )
        }
    },
    delayMillis = 500,
    tooltipPlacement = TooltipPlacement.ComponentRect(
        alignment = Alignment.BottomEnd,
    ),
    content = content
)