import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.withTransform
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.input.pointer.PointerEventType
import androidx.compose.ui.input.pointer.onPointerEvent
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.toOffset
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlin.math.exp
import kotlin.math.min
import kotlin.math.sign

fun Size.toIntSize(): IntSize = IntSize(width.toInt(), height.toInt())

fun <T> Field<T>.checkCoordinates(col: Int, row: Int): Boolean =
    (row in (0 until height)) && (col in (0 until width))

fun GameOfLifeViewModel.scale(delta: Int) {
    scale = (scale * exp(delta * 0.2f)).coerceIn(0.5f, 10f)
}

fun GameOfLifeViewModel.offset(delta: Offset) {
    offset = if (scale == 1f) Offset.Zero else offset + delta
}

fun GameOfLifeViewModel.resetGeometry() {
    scale = 1f
    offset = Offset.Zero
}

enum class PanDirection(val offset: Offset) {
    Left(Offset(1f, 0f)), Right(Offset(-1f, 0f)), Up(Offset(0f, 1f)), Down(Offset(0f, -1f))
}

private val logger = KotlinLogging.logger {}

private fun offsetToCoordinates(offset: Offset, cellSize: Size): Pair<Int, Int> {
    val curOffset = with(offset) {
        Offset(this.x / cellSize.width, this.y / cellSize.height)
    }
    logger.trace { "Click at $curOffset" }

    return curOffset.x.toInt() to curOffset.y.toInt()  // note that coordinates are transposed
}

@Composable
private fun CellInfo(cell: Cell?) = Text(
    text = cell?.let { "Alive: ${cell.alive}" + if (cell.alive) "; Generations: ${cell.generations}" else "" }
        ?: "No cell selected",
    style = MaterialTheme.typography.body1,
)

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun GameOfLifeFieldView(state: GameOfLifeViewModel) {
    val field = state.field

    var cellSize by remember { mutableStateOf(Size.Zero) }
    var alignOffset by remember { mutableStateOf(Offset.Zero) }
    var hoveredCell: Cell? by remember { mutableStateOf(null) }

    Column(modifier = Modifier.fillMaxWidth()) {
        Canvas(
            modifier = Modifier.fillMaxSize().weight(1.0f).align(Alignment.CenterHorizontally)
                .clipToBounds()
                .graphicsLayer {
                    scaleX = state.scale
                    scaleY = state.scale

                    translationX = state.offset.x
                    translationY = state.offset.y
                }
                .onPointerEvent(PointerEventType.Press) {
                    val clickedOffset = it.changes.firstOrNull()?.position ?: Offset.Zero

                    val (curRow, curCol) = offsetToCoordinates(clickedOffset - alignOffset, cellSize)
                    if (field.checkCoordinates(curRow, curCol)) {
                        logger.debug {
                            if (field[curRow, curCol].alive)
                                "Killing cell at ($curRow, $curCol)" else "Bringing cell at ($curRow, $curCol) back to life"
                        }
                        field[curRow, curCol].switch()
                    } else {
                        logger.debug { "Click at $clickedOffset outside of the field" }
                    }
                }.onPointerEvent(PointerEventType.Move) {
                    val hoveredOffset = it.changes.firstOrNull()?.position ?: Offset.Zero

                    val (curRow, curCol) = offsetToCoordinates(hoveredOffset - alignOffset, cellSize)
                    hoveredCell = if (field.checkCoordinates(curRow, curCol))
                        field[curRow, curCol]
                    else
                        null
                }.onPointerEvent(PointerEventType.Scroll) {
                    val event = it.changes.first()

                    state.scale(-event.scrollDelta.y.toInt().sign)
                }
        ) {
            cellSize = min(
                size.width / state.game.fieldWidth,
                size.height / state.game.fieldHeight
            ).run { Size(this, this) }

            alignOffset = Alignment.Center.align(
                Size(cellSize.width * state.game.fieldWidth, cellSize.height * state.game.fieldHeight).toIntSize(),
                size.toIntSize(),
                layoutDirection
            ).toOffset()

            withTransform({
                translate(alignOffset.x, alignOffset.y)
            })
            {
                field.forEachIndexed { rowN, row ->
                    row.forEachIndexed { colN, cell ->
                        drawRect(
                            color = if (cell.alive) Color.Black else Color.White,
                            size = cellSize,
                            topLeft = Offset(cellSize.width * rowN, cellSize.height * colN)
                        )
                    }
                }
            }
        }

        CellInfo(hoveredCell)
    }
}