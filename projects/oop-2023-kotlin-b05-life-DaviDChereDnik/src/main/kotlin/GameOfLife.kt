import io.github.oshai.kotlinlogging.KotlinLogging
import kotlin.math.abs
import kotlin.properties.Delegates

fun Field<Cell>.sync(field: Field<Cell>) {
    field.forEachCellIndexed { rowN, colN, cell ->
        this[rowN, colN].alive = cell.alive
        this[rowN, colN].generations = cell.generations
    }
}

private val logger = KotlinLogging.logger {}

class GameOfLife(
    val fieldHeight: Int = 1024, val fieldWidth: Int = 1024,

    // Classic rules: https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life#Rules
    var liveRules: MutableSet<Int> = mutableSetOf(2, 3), var reviveRules: MutableSet<Int> = mutableSetOf(3),

    fieldType: FieldAccessor<Cell> = fieldTypes.Toroidal,

    fieldSeed: (row: Int, col: Int) -> Cell = { _, _ -> Cell() },

    // https://en.wikipedia.org/wiki/Moore_neighborhood
    var mooreDistance: Int = 1
) {
    val field: Field<Cell> = Field(fieldHeight, fieldWidth, fieldType, { Cell() }, fieldSeed)
    private val nextField: Field<Cell> = Field(fieldHeight, fieldWidth, fieldType, { Cell() })

    var generationCount = 0
        private set

    var fieldType: FieldAccessor<Cell> by Delegates.observable(fieldType) { _, _, new ->
        logger.debug { "Changing field type from ${this.field} to $new" }

        this.field.fieldAccessor = new
        this.nextField.fieldAccessor = new
    }

    init {
        logger.info { "Initialized new Game Of Life on the ${fieldWidth}x${fieldHeight} field" }
    }

    fun nextGeneration() {
        generationCount++
        logger.debug { "Proceeding generation no. $generationCount" }

        nextField.sync(field)

        field.forEachCellIndexed { rowN, colN, cell ->
            val curSum = countAliveNeighbours(field, rowN, colN, mooreDistance)
            val nextCell = nextField[rowN, colN]

            val cellMessage by lazy { "$cell at ($rowN, $colN) with $curSum neighbours" }

            if (cell.alive) {
                if (curSum in liveRules) {
                    logger.trace { "$cellMessage lives up to the next generation" }
                    nextCell.generations++
                } else {
                    logger.trace { "$cellMessage dies" }
                    nextCell.alive = false
                    nextCell.generations = 0
                }
            } else if (curSum in reviveRules) {
                logger.trace { "$cellMessage returns to the life" }
                nextCell.alive = true
            } else {
                logger.trace { "$cellMessage keeps being dead" }
            }
        }

        field.sync(nextField)
    }

    override fun toString(): String = buildString {
        field.forEachCellIndexed { _, colN, cell ->
            append(if (cell.alive) '▣' else '□')

            // columns (as everything) are numbered from 0
            if (colN + 1 == fieldWidth) append('\n')
        }
    }

    companion object {
        val fieldTypes = FieldType<Cell>()

        internal fun countAliveNeighbours(field: Field<Cell>, row: Int, col: Int, mooreDistance: Int): Int {
            var sum = 0

            (-mooreDistance..mooreDistance).forEach { dRow ->
                (-mooreDistance..mooreDistance).forEach { dCol ->
                    if (abs(dCol) + abs(dRow) != 0 && field[row + dRow, col + dCol].alive) sum += 1
                }
            }

            return sum
        }
    }
}