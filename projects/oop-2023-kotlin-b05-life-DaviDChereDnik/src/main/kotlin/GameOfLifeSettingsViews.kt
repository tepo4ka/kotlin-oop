import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Checkbox
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.ListItem
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Contactless
import androidx.compose.material.icons.filled.Living
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material.icons.filled.SocialDistance
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.runtime.toMutableStateList
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import kotlin.math.max

fun GameOfLife.neighboursAmount() = (2 * mooreDistance + 1) * (2 * mooreDistance + 1) - 1

@Composable
private fun DropdownMenuRules(currentRules: MutableSet<Int>, availableRange: IntRange) {
    var isExpanded by remember { mutableStateOf(false) }

    // inspiration was taken from https://stackoverflow.com/questions/75397960/jetpack-compose-combo-box-with-dropdown
    // it's harder than it seems to be to track MutableSet for recompositoins, so MutableStateList is used, which,
    // unfortunately, brings more time complexity
    val selectedRules = remember { currentRules.toMutableStateList() }

    Box(contentAlignment = Alignment.Center) {
        TextButton(onClick = { isExpanded = true }) {
            Text("$currentRules")
        }
        DropdownMenu(expanded = isExpanded, onDismissRequest = {
            isExpanded = false

            currentRules.clear()
            currentRules.addAll(selectedRules)
            currentRules.sorted()
        }) {
            availableRange.forEach { n ->
                val checked by remember { derivedStateOf { n in selectedRules } }

                DropdownMenuItem(onClick = {
                    if (checked)
                        selectedRules.remove(n)
                    else
                        selectedRules.add(n)
                }) {
                    Row(verticalAlignment = Alignment.CenterVertically) {
                        Checkbox(checked = checked, onCheckedChange = { newState ->
                            if (!newState)
                                selectedRules.remove(n)
                            else
                                selectedRules.add(n)
                        })

                        Spacer(modifier = Modifier.width(5.dp))

                        Text(text = "$n")
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
private fun MooreDistanceSettings(state: GameOfLifeViewModel) = ListItem(
    text = { Text("Moore distance") },
    secondaryText = { Text("How many neighbours to consider (from one side)") },
    icon = { Icon(Icons.Filled.SocialDistance, "") },
    trailing = {
        var mooreDistance by remember { mutableStateOf(state.game.mooreDistance) }

        Box(modifier = Modifier.width(50.dp)) {
            OutlinedTextField(
                value = mooreDistance.toString(),
                onValueChange = { newValue -> mooreDistance = max(1, newValue.toIntOrNull() ?: 1) },
                singleLine = true,
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
            )
        }
    }
)

@OptIn(ExperimentalMaterialApi::class)
@Composable
private fun LiveRulesSettings(state: GameOfLifeViewModel) = ListItem(
    text = { Text("Live rules") },
    secondaryText = { Text("How much alive neighbours should be for cell to live") },
    icon = { Icon(Icons.Filled.Living, "") },
    trailing = {
        DropdownMenuRules(state.game.liveRules, (1..state.game.neighboursAmount()))
    }
)

@OptIn(ExperimentalMaterialApi::class)
@Composable
private fun ReviveRulesSettings(state: GameOfLifeViewModel) = ListItem(
    text = { Text("Revive rules") },
    secondaryText = { Text("How much alive neighbours should be for cell to return from death") },
    icon = { Icon(Icons.Filled.Contactless, "") },
    trailing = {
        DropdownMenuRules(state.game.reviveRules, (1..state.game.neighboursAmount()))
    }
)

@Composable
private fun SettingsDialog(state: GameOfLifeViewModel, title: String, onCloseRequest: () -> Unit) {
    Dialog(
        title = title,
        onCloseRequest = onCloseRequest,
    ) {
        Column {
            MooreDistanceSettings(state)

            LiveRulesSettings(state)
            ReviveRulesSettings(state)
        }
    }
}

@Composable
fun GameOfLifeSettings(state: GameOfLifeViewModel) {
    var settingsOpened by remember { mutableStateOf(false) }

    PlainTooltip("Open GOF settings") {
        IconButton(onClick = { settingsOpened = true }) {
            Icon(Icons.Filled.Settings, "")
        }
    }

    if (settingsOpened) {
        SettingsDialog(state, "GOF settings") { settingsOpened = false }
    }
}