import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.graphics.Color

class Cell(alive: Boolean = false, var generations: Int = 0) {
    var alive by mutableStateOf(alive)
}

fun Cell.switch() {
    alive = !alive
}