// https://whatheco.de/2020/05/05/list-of-strongly-typed-objects-acting-like-enum-in-kotlin/
open class EnumObject<T>
private constructor(private val list: MutableList<T>) : List<T> by list {
    constructor() : this(mutableListOf())

    protected fun <TAdd : T> add(item: TAdd): TAdd = item.also { list.add(it) }
}

interface FieldAccessor<E> {
    fun get(row: Int, col: Int, defaultValue: () -> E, field: List<List<E>>): E
}

class FieldType<E> : EnumObject<FieldAccessor<E>>() {
    val Toroidal = add(object : FieldAccessor<E> {
        // Int.mod is used here because default module operator is mathematically incorrect (e.g. -2 % 9 == -2)
        override fun get(row: Int, col: Int, defaultValue: () -> E, field: List<List<E>>): E =
            field[row.mod(field.size)][col.mod(field.first().size)]
    })

    val Finite = add(object : FieldAccessor<E> {
        override fun get(row: Int, col: Int, defaultValue: () -> E, field: List<List<E>>): E =
            if (row in field.indices && col in field.first().indices) field[row][col] else defaultValue()
    })
}