import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.NotStarted
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material.icons.filled.Stop
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.KeyboardType
import kotlin.math.max

@Composable
private fun GenerationAmountField(state: GameOfLifeViewModel) {
    OutlinedTextField(
        value = state.generateAmount.toString(),
        onValueChange = { newValue ->
            state.generateAmount = max(0, newValue.toIntOrNull() ?: 0)
        },
        readOnly = state.proceedGenerate,
        singleLine = true,
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
        label = { Text(text = "Run N generations") }
    )
}

@Composable
fun GameOfLifeFiniteGenerate(state: GameOfLifeViewModel) {
    GenerationAmountField(state)

    PlainTooltip(
        if (state.proceedGenerate)
            "Pause proceeding of generations" else "Proceed specified amount of generations"
    ) {
        IconButton(
            onClick = { if (state.generateAmount > 0) state.proceedGenerate = !state.proceedGenerate },
            enabled = !state.infiniteGenerate
        ) {
            Icon(
                if (state.proceedGenerate) Icons.Filled.Stop else Icons.Filled.PlayArrow,
                "",
                tint = if (state.proceedGenerate) Color.Green else Color.Black
            )
        }
    }
}

@Composable
fun GameOfLifeInfiniteGenerate(state: GameOfLifeViewModel) = PlainTooltip(
    if (state.infiniteGenerate)
        "Stop proceeding new generations" else "Proceed new generations until stopped"
) {
    IconButton(onClick = { state.infiniteGenerate = !state.infiniteGenerate }, enabled = !state.proceedGenerate) {
        Icon(
            if (state.infiniteGenerate) Icons.Filled.Stop else Icons.Filled.NotStarted,
            "",
            tint = if (state.infiniteGenerate) Color.Red else Color.Black
        )
    }
}
