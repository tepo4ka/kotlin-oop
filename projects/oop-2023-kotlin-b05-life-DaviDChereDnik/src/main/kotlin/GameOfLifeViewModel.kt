import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.geometry.Offset
import kotlinx.coroutines.Delay

fun GameOfLife.reseed(seed: GameOfLifeSeed) = field.forEachCellIndexed { rowN, colN, cell ->
    val curCell = seed.seed(rowN, colN)
    cell.alive = curCell.alive
    cell.generations = curCell.generations
}

class GameOfLifeViewModel(val game: GameOfLife) {
    val field by mutableStateOf(game.field)

    var infiniteGenerate by mutableStateOf(false)

    var proceedGenerate by mutableStateOf(false)
    var generateAmount by mutableStateOf(0)

    val generateDelay = 500L

    var scale by mutableStateOf(1f)
    var offset by mutableStateOf(Offset.Zero)

    val panOffset = 20f
}