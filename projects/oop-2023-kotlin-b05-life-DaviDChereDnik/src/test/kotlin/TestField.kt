import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

internal class TestField {
    private val repeatRandom = 30
    private fun randomSize() = (1..100).random()
    private fun randomFrom(from: Int) = Random.nextInt(from, Int.MAX_VALUE)

    private fun <T> getOuterElement(field: Field<T>): T =
        field[randomFrom(field.size), randomFrom(field.first().size)]

    data class IntBox(var value: Int = 0)

    @Test
    fun `test field default value with all types`() {
        for (fieldType in FieldType<Int>()) {
            val currentDefault = Random.nextInt()
            val field = Field(randomSize(), randomSize(), fieldType, { currentDefault })

            field.forEachCell { cell ->
                assertEquals(currentDefault, cell)
            }
        }
    }

    @Test
    fun `test field default value with all types and new get`() {
        FieldType<Int>().forEach { fieldType ->
            val currentDefault = Random.nextInt()
            val field = Field(randomSize(), randomSize(), fieldType, { currentDefault })

            field.forEachCellIndexed { rowN, colN, _ -> assertEquals(currentDefault, field[rowN, colN]) }
        }
    }

    @Test
    fun `test field with non-default initialization`() {
        FieldType<Int>().forEach { fieldType ->
            val currentDefault = Random.nextInt()
            val field = Field(randomSize(), randomSize(), fieldType, { currentDefault }, { row, col -> row + col })

            field.forEachCellIndexed { rowN, colN, cell -> assertEquals(rowN + colN, cell) }
        }
    }

    @Test
    fun `test toroidal field correctness`() {
        val field = Field(randomSize(), randomSize(), FieldType<IntBox>().Toroidal, { IntBox(0) })

        repeat(repeatRandom) {
            val currentRow = Random.nextInt()
            val currentCol = Random.nextInt()

            val oldValue = field[currentRow, currentCol].value

            field[currentRow, currentCol].value = Random.nextInt()

            assertNotEquals(oldValue, field[currentRow, currentCol].value)
        }
    }

    @Test
    fun `test finite field correctness`() {
        val defaultValue = IntBox(0)
        val field = Field(randomSize(), randomSize(), FieldType<IntBox>().Finite, { IntBox(0) })

        repeat(repeatRandom) {
            val currentRow = (0 until field.size).random()
            val currentCol = (0 until field.first().size).random()

            val oldValue = field[currentRow, currentCol].value

            field[currentRow, currentCol].value = Random.nextInt()

            assertNotEquals(oldValue, field[currentRow, currentCol].value)
            assertEquals(defaultValue.value, field[currentRow + field.size, currentCol + field.first().size].value)
        }
    }

    @Test
    fun `test that finite field gives different default values`() {
        val field = Field(randomSize(), randomSize(), FieldType<IntBox>().Finite, { IntBox(0) })

        repeat(repeatRandom) {
            val default1 = getOuterElement(field)
            val default2 = getOuterElement(field)

            assertEquals(default1.value, default2.value)

            default1.value = Random.nextInt()
            assertNotEquals(default1, default2)
        }
    }
}