import kotlin.math.min
import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertEquals

internal class TestGameOfLife {
    private fun getGameForPattern(pattern: GameOfLifePattern, addHeight: Int = 0, addWidth: Int = 0) =
        GameOfLife(pattern.region.size + 2 + addHeight, pattern.region.first().size + 2 + addWidth)

    private fun randomFrom(from: Int) = Random.nextInt(from, Int.MAX_VALUE)

    private val repeatTimes = 30

    @Test
    fun `test that everything outside of the finite field is dead`() {
        val game = GameOfLife(
            fieldHeight = Random.nextInt(1, 1024),
            fieldWidth = Random.nextInt(1, 1024),
            fieldType = GameOfLife.fieldTypes.Finite,
            fieldSeed = { _, _ -> Cell(alive = true) })

        repeat(repeatTimes) {
            assertEquals(
                Cell(alive = true),
                game.field[Random.nextInt(game.fieldHeight), Random.nextInt(game.fieldWidth)]
            )
            assertEquals(
                Cell(alive = false),
                game.field[randomFrom(game.fieldHeight), randomFrom(game.fieldWidth)]
            )
            assertEquals(
                Cell(alive = false),
                game.field[-randomFrom(game.fieldHeight), -randomFrom(game.fieldWidth)]
            )
        }
    }

    @Test
    fun `test that game's field type change reflects the underlying field`() {
        repeat(repeatTimes) {
            val game = GameOfLife(
                fieldHeight = Random.nextInt(1, 1024),
                fieldWidth = Random.nextInt(1, 1024),
                fieldSeed = { _, _ -> Cell(alive = true) })

            game.fieldType = GameOfLife.fieldTypes.Toroidal
            assertEquals(Cell(alive = true), game.field[Random.nextInt(), Random.nextInt()])
            assertEquals(Cell(alive = true), game.field[-Random.nextInt(), -Random.nextInt()])

            game.fieldType = GameOfLife.fieldTypes.Finite
            assertEquals(Cell(alive = false), game.field[randomFrom(game.fieldHeight), randomFrom(game.fieldWidth)])
            assertEquals(Cell(alive = false), game.field[-randomFrom(game.fieldHeight), -randomFrom(game.fieldWidth)])
        }
    }

    @Test
    fun `test countAliveNeighbours with trivial cases`() {
        val gameAllAlive = GameOfLife(
            fieldHeight = 5,
            fieldWidth = 5,
            fieldSeed = { _, _ -> Cell(alive = true) })

        assertEquals(8, GameOfLife.countAliveNeighbours(gameAllAlive.field, 2, 2, 1))
        assertEquals(24, GameOfLife.countAliveNeighbours(gameAllAlive.field, 2, 2, 2))

        gameAllAlive.fieldType = GameOfLife.fieldTypes.Toroidal
        assertEquals(8, GameOfLife.countAliveNeighbours(gameAllAlive.field, 0, 0, 1))

        gameAllAlive.fieldType = GameOfLife.fieldTypes.Finite
        assertEquals(3, GameOfLife.countAliveNeighbours(gameAllAlive.field, 0, 0, 1))

        val gameAllDead = GameOfLife(
            fieldHeight = 5,
            fieldWidth = 5,
            fieldSeed = { _, _ -> Cell(alive = false) })
        assertEquals(0, GameOfLife.countAliveNeighbours(gameAllDead.field, 2, 2, 1))
        assertEquals(0, GameOfLife.countAliveNeighbours(gameAllDead.field, 0, 0, 1))
        assertEquals(0, GameOfLife.countAliveNeighbours(gameAllDead.field, 2, 2, 2))

        gameAllDead.fieldType = GameOfLife.fieldTypes.Finite
        assertEquals(0, GameOfLife.countAliveNeighbours(gameAllDead.field, 0, 0, 1))
    }

    @Test
    fun `test countAliveNeighbours with real-world cases`() {
        repeat(repeatTimes) {
            // integrate random block with known neighbours amount and check if function's result is the same

            // everything is checked with the toroidal field type as it's assumed that if toroidal one works correctly,
            // then so does the finite one (and, maybe, others, if they are also correct)

            // maybe this test is more complicated than it needs to be

            val game = GameOfLife(
                // we should have at least field 3x3, because otherwise moore distance will have to be 0
                fieldHeight = Random.nextInt(3, 1024),
                fieldWidth = Random.nextInt(3, 1024),
                fieldSeed = GameOfLifeSeed.`Kind random`.seed
            )

            // area of the neighbourhood with given mooreDistance is (2 * mooreDistance + 1)^2, so we should be able to fit it on the field,
            // hence the restrictions
            val mooreDistance = Random.nextInt(1, min((game.fieldHeight - 1) / 2, (game.fieldWidth - 1) / 2) + 1)

            val knownNeighbours =
                Field(
                    height = 2 * mooreDistance + 1,
                    width = 2 * mooreDistance + 1,
                    fieldAccessor = FieldType<Int>().random(),
                    defaultValue = { Random.nextInt(2) })  // either 1 (alive cell) or 0 (dead cell)

            // central (main) cell doesn't count as neighbour of itself
            val knownNeighboursAmount =
                knownNeighbours.sumOf { row -> row.sumOf { it } } - knownNeighbours[mooreDistance, mooreDistance]

            val placeRow = Random.nextInt()
            val placeCol = Random.nextInt()

            knownNeighbours.forEachCellIndexed { rowN, colN, cell ->
                game.field[rowN + placeRow, colN + placeCol].alive = cell == 1
            }

            assertEquals(
                knownNeighboursAmount,
                GameOfLife.countAliveNeighbours(
                    game.field,
                    placeRow + mooreDistance,
                    placeCol + mooreDistance,
                    mooreDistance
                )
            )
        }
    }

    @Test
    fun `test Stills patterns "stillness" behavior`() {
        GameOfLifeDefaultPatterns.entries.asSequence().filter { it.pattern.type == GameOfLifePatternType.Still }
            .forEach {
                val pattern = it.pattern
                val game = getGameForPattern(pattern)
                game.placePattern(pattern, 1, 1)

                val initialField = game.field

                repeat(repeatTimes) {
                    game.nextGeneration()
                    assertEquals(initialField, game.field)
                }
            }
    }

    @Test
    fun `test Oscillators pattern periodic behavior`() {
        GameOfLifeDefaultPatterns.entries.asSequence().filter { it.pattern.type == GameOfLifePatternType.Oscillator }
            .forEach { entry ->
                val pattern = entry.pattern
                val game = getGameForPattern(pattern)
                game.placePattern(pattern, 1, 1)

                val states: List<List<List<Boolean>>> = buildList {
                    repeat(Random.nextInt(pattern.period, pattern.period + repeatTimes)) {
                        add(game.field.map { row -> row.map { cell -> cell.alive } })
                        game.nextGeneration()
                    }
                }.distinct()

                assertEquals(pattern.period, states.size)
            }
    }

    @Test
    fun `test that Spaceships do not ever die`() {
        GameOfLifeDefaultPatterns.entries.asSequence().filter { it.pattern.type == GameOfLifePatternType.Spaceship }
            .forEach { entry ->
                val pattern = entry.pattern
                val game = getGameForPattern(pattern)
                game.placePattern(pattern, 1, 1)

                val countAliveCells =
                    // https://youtrack.jetbrains.com/issue/KT-46360
                    { field: Field<Cell> -> field.sumOf { row -> row.sumOf { cell -> if (cell.alive) 1.toInt() else 0.toInt() } } }

                val initialCount = countAliveCells(game.field)

                repeat(repeatTimes) {
                    game.nextGeneration()
                    assertEquals(initialCount, countAliveCells(game.field))
                }
            }
    }

    @Test
    fun `test generations count of alive cell`() {
        val pattern = GameOfLifeDefaultPatterns.Block.pattern
        val game = getGameForPattern(pattern)
        game.placePattern(pattern, 1, 1)

        repeat(repeatTimes) {
            assertEquals(it, game.field[1, 1].generations)
            game.nextGeneration()
        }
    }

    @Test
    fun `test generations count of revived cell`() {
        val pattern = GameOfLifeDefaultPatterns.Beacon.pattern
        val game = getGameForPattern(pattern)
        game.placePattern(pattern, 1, 1)

        repeat(repeatTimes) {
            // in blinker, cell (3, 3) becomes dead every 2 steps, so it can't live longer than 2 generation
            assertEquals(0, game.field[2, 2].generations)
        }
    }
}