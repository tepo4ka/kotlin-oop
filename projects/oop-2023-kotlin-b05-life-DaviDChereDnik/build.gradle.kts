import org.jetbrains.compose.desktop.application.dsl.TargetFormat

plugins {
    kotlin("jvm")

    id("org.jetbrains.compose")
}

group = "ru.spbu.math-cs"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()

    maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
    google()
}

dependencies {
    implementation(kotlin("stdlib"))
    testImplementation(kotlin("test"))

    implementation(compose.desktop.currentOs)
    implementation(compose.materialIconsExtended)

    implementation("io.github.oshai:kotlin-logging-jvm:5.1.0")
    runtimeOnly("org.slf4j:slf4j-simple:2.0.3")
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}

compose.desktop {
    application {
        mainClass = "MainKt"

        nativeDistributions {
            targetFormats(TargetFormat.Msi, TargetFormat.Deb)
            packageName = "pf-2023-kotlin-life-DaviDChereDnik"
            packageVersion = "0.0.1"
        }
    }
}