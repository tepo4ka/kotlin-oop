import kotlin.test.*

internal class Phase3Test {
    @Test
    fun testOperators() {
        val r1 = Rational(1,2)
        val r2 = Rational(1,3)
        assertEquals(Rational(5, 6), r1 + r2)
        assertEquals(Rational(1, 6), r1 - r2)
        assertEquals(Rational(1, 6), r1 * r2)
        assertEquals(Rational(3, 2), r1 / r2)
        assertEquals(Rational(11, 4), 2 + Rational(3, 4))
        assertEquals(Rational(5, 4), 2 - Rational(3, 4))
        assertEquals(Rational(6, 4), 2 * Rational(3, 4))
        assertEquals(Rational(8, 3), 2 / Rational(3, 4))
        assertTrue(r1 > r2)
        assertTrue(r1 >= r1)
        assertTrue(r1 != -r1)
    }
}
