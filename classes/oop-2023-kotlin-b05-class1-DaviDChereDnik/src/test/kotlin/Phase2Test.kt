import kotlin.test.*

internal class Phase2Test {
    @Test
    fun testSecondaryConstructors() {
        val r = Rational(-4, 3)
        assertEquals(r, Rational(-1, 1, 3))
        assertEquals(r, Rational(0, -4, 3))
        assertEquals(r, Rational(r))
        assertEquals(r, Rational("-4/3"))
        assertEquals(r, Rational("-12/9"))
    }
}