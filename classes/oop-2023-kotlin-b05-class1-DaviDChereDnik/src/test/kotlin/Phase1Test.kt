import kotlin.test.*

internal class Phase1Test {
    @Test
    fun testCanonicity() {
        assertEquals("1/2", Rational(2, 4).toString())
        assertEquals("-1/3", Rational(1, -3).toString())
        assertEquals("2/5", Rational(-8, -20).toString())
        assertEquals("17/11", Rational(17, 11).toString())
    }
    @Test
    fun testToString() {
        assertEquals("2", Rational(4, 2).toString())
        assertEquals("0", Rational(0, 10).toString())
        assertEquals("10", Rational(100, 10).toString())
    }
}