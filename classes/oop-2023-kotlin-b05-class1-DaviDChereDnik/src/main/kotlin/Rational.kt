import kotlin.math.abs
import kotlin.math.sign

class Rational(numerator: Int, denominator: Int) {
    val n: Int // numerator: integer
    val d: Int // denominator: positive integer

    init {
        if (denominator == 0) throw ArithmeticException("Denominator can't be 0")

        val g = gcd(abs(numerator), abs(denominator))
        n = ((numerator.sign * denominator.sign) * abs(numerator)) / g
        d = abs(denominator) / g
    }

    constructor(whole: Int, numerator: Int, denominator: Int) : this(
        whole * denominator + numerator * (if (whole != 0) whole.sign else 1), denominator
    )

    constructor(numerator: Int) : this(numerator, 1)

    constructor(r: Rational) : this(r.n, r.d)

    constructor(r: String) : this(stringToRational(r))

    companion object {
        fun stringToRational(s: String): Rational {
            val nd = s.split("/")
            if (nd.size != 2) throw IllegalArgumentException("Incorrect fraction $s: should be n/d")

            return Rational(nd[0].toInt(), nd[1].toInt())
        }

        fun gcd(a: Int, b: Int): Int {
            if (b == 0) return a
            return gcd(b, a % b)
        }
    }

    val wholePart: Int by lazy {
        n / d
    }

    val properPart: Rational by lazy {
        Rational(n - d * wholePart, d)
    }

    val isWhole: Boolean by lazy {
        d == 1
    }

    val isZero: Boolean by lazy {
        n == 0
    }

    override fun toString(): String {
        return if (isWhole) "$n"
        else "$n/$d"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Rational

        if (n != other.n) return false
        if (d != other.d) return false

        return true
    }

    override fun hashCode(): Int {
        var result = n
        result = 31 * result + d
        return result
    }

    operator fun unaryPlus(): Rational = this
    operator fun unaryMinus(): Rational = Rational(-n, d)
    operator fun plus(r: Rational): Rational = Rational(n * r.d + r.n * d, r.d * d)
    operator fun plus(i: Int): Rational = this + Rational(i)
    operator fun inc(): Rational = this + 1
    operator fun minus(r: Rational): Rational = this + (-r)
    operator fun minus(i: Int): Rational = this - Rational(i)
    operator fun dec(): Rational = this - 1
    operator fun times(r: Rational): Rational = Rational(n * r.n, d * r.d)
    operator fun times(i: Int): Rational = this * Rational(i)
    operator fun div(r: Rational): Rational = Rational(n * r.d, d * r.n)
    operator fun div(i: Int): Rational = this / Rational(i)

    operator fun compareTo(r: Rational): Int {
        val t = this - r
        return t.n // since d is always a positive integer
    }

    operator fun compareTo(i: Int): Int {
        val t = this - Rational(i)
        return t.n
    }
}

operator fun Int.plus(r: Rational): Rational = Rational(this) + r
operator fun Int.minus(r: Rational): Rational = Rational(this) - r
operator fun Int.div(r: Rational): Rational = Rational(this) / r
operator fun Int.times(r: Rational): Rational = Rational(this) * r