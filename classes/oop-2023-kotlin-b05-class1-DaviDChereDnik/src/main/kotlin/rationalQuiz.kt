fun rationalQuiz(showAnswer: Boolean = false, nRange: IntRange = 1..100, dRange: IntRange = 1..100): String {
    val r1 = Rational(nRange.random(), dRange.random())
    val r2 = Rational(nRange.random(), dRange.random())

    val equation =
        listOf(toMixedVulgarFraction(r1), toMixedVulgarFraction(r2), toMixedVulgarFraction(r1 + r2)).toMutableList()

    if (!showAnswer) {
        equation[equation.indices.random()] = "?"
    }

    return "${equation[0]} + ${equation[1]} = ${equation[2]}"
}