fun multiplicationTable(n: Int): String {
    val result = StringBuilder()

    val multipliers = buildList {
        for (i in 1 until n)
            add(Rational(i, n))
    }
    val table: List<List<Rational>> = buildList {
        for (row in multipliers) {
            add(buildList {
                for (column in multipliers) {
                    add(row * column)
                }
            })
        }
    }

    val rowWidth = table.maxOf { row ->
        row.maxOf { column ->
            column.toString().length
        }
    }
    val tableCell = { cell: Rational ->
        cell.toString().padEnd(rowWidth)
    }

    result.append(" ".repeat(rowWidth))
    multipliers.forEach { m ->
        result.append(" ")
            .append(tableCell(m))
    }

    result.append("\n")
        .append("─".repeat(result.length - 1))
        .append("\n")

    table.forEachIndexed { m, row ->
        result.append(tableCell(multipliers[m]))
        row.forEach { cell ->
            result.append(" ")
                .append(tableCell(cell))
        }
        result.append("\n")
    }

    return result.toString()
}