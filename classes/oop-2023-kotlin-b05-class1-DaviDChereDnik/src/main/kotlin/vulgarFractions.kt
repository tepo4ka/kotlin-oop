val vulgarFractions = mapOf(
    Rational(1, 4) to '\u00BC',
    Rational(1, 2) to '\u00BD',
    Rational(3, 4) to '\u00BE',
    Rational(1, 7) to '\u2150',
    Rational(1, 9) to '\u2151',
    Rational(1, 10) to '\u2152',
    Rational(1, 3) to '\u2153',
    Rational(2, 3) to '\u2154',
    Rational(1, 5) to '\u2155',
    Rational(2, 5) to '\u2156',
    Rational(3, 5) to '\u2157',
    Rational(4, 5) to '\u2158',
    Rational(1, 6) to '\u2159',
    Rational(5, 6) to '\u215A',
    Rational(1, 8) to '\u215B',
    Rational(3, 8) to '\u215C',
    Rational(5, 8) to '\u215D',
    Rational(7, 8) to '\u215E'
)

const val superscripts = "⁰¹²³⁴⁵⁶⁷⁸⁹"
const val subscripts = "₀₁₂₃₄₅₆₇₈₉"

fun toMixedVulgarFraction(r: Rational): String {
    val result = StringBuilder()

    if (r.wholePart != 0 || r.isZero)
        result.append(r.wholePart)

    if (!r.isWhole) {
        if (r.properPart in vulgarFractions) {
            result.append(vulgarFractions[r.properPart])
        } else {
            r.properPart.n.toString().forEach { digit ->
                result.append(superscripts[digit.code - 48])
            }
            result.append("/")
            r.properPart.d.toString().forEach { digit ->
                result.append(subscripts[digit.code - 48])
            }
        }
    }

    return result.toString()
}