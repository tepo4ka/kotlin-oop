import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.6.20"
    application
}

group = "ru.spbu.math-cs"
version = "1.0"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
    testImplementation(kotlin("test-junit"))
}

tasks.test {
    useJUnit()
}


application {
    mainClass.set("MainKt")
}


tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}