import java.io.File

fun main() {
    val plays = PlayCsvLoader.load(File("plays.csv").inputStream())
    println(
        Statement.create("html").render(
            Invoice(
                "BigCo",
                PerformanceCsvLoader.load(File("invoice.csv").inputStream(), plays)
            )
        )
    )

    println("Processing script.thsh")
    TheaterShell().run(File("script.thsh").inputStream())
//    TheaterShell.process(listOf("load", "plays", "csv", "plays.csv"))
}