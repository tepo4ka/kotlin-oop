data class StatementLine(
    val playName: String, val amount: Int,
    val volumeCredits: Int, val audience: Int
) {
    override fun toString(): String {
        return " $playName: ${asUSD(amount)} (${audience} seats)"
    }
}

data class StatementData(
    val customer: String,
    val statementLines: List<StatementLine>
) {
    val volumeCredits = statementLines.sumOf { it.volumeCredits }
    val totalAmount = statementLines.sumOf { it.amount }
}

interface Statement {
    fun render(invoice: Invoice): String = renderer.render(statementData(invoice))
    val renderer: StatementDataRenderer

    companion object {
        fun create(renderer: String): Statement = when (renderer.lowercase()) {
            "text" -> PlainTextStatement
            "html" -> HtmlStatement
            else -> throw IllegalArgumentException("Unsupported renderer type")
        }
    }
}

object PlainTextStatement : Statement {
    override val renderer = PlainTextStatementDataRenderer
}

object HtmlStatement : Statement {
    override val renderer = HtmlStatementDataRenderer
}

private fun statementData(invoice: Invoice): StatementData {
    fun statementLine(perf: Performance): StatementLine {
        val play = perf.play
        return StatementLine(
            play.name, amountFor(perf, play),
            volumeCreditsFor(perf, play), perf.audience
        )
    }

    return StatementData(
        invoice.customer,
        invoice.performances.map(::statementLine)
    )
}