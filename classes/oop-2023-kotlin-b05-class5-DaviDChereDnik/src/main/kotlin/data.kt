data class Play(val id: String, val name: String, val calculator: PlayCalculator)
data class Performance(val play: Play, val audience: Int)
data class Invoice(val customer: String, val performances: List<Performance>)

val PLAYS = listOf(
    Play("hamlet", "Hamlet", TragedyCalculator()),
    Play("as-like", "As You Like it", ComedyCalculator()),
    Play("othello", "Othello", TragedyCalculator())
)

val INVOICE = Invoice(
    "BigCo", listOf(
        Performance(PLAYS[0], 55),
        Performance(PLAYS[1], 35),
        Performance(PLAYS[2], 40)
    )
)