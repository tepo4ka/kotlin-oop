import kotlinx.html.META
import kotlinx.html.body
import kotlinx.html.dom.createHTMLDocument
import kotlinx.html.dom.serialize
import kotlinx.html.h1
import kotlinx.html.head
import kotlinx.html.html
import kotlinx.html.meta
import kotlinx.html.p
import kotlinx.html.table
import kotlinx.html.td
import kotlinx.html.thead
import kotlinx.html.title
import kotlinx.html.tr
import java.text.NumberFormat
import java.util.*

interface StatementDataRenderer {
    fun render(statementData: StatementData): String
}

object PlainTextStatementDataRenderer : StatementDataRenderer {
    override fun render(statementData: StatementData): String {
        val result = StringBuilder("Statement for ${statementData.customer}\n")
        statementData.statementLines.forEach {
            result.appendLine(it)
        }
        result.append("Amount owed is ${asUSD(statementData.totalAmount)}\n")
        result.append("You earned ${statementData.volumeCredits} credits\n")
        return result.toString()
    }
}

object HtmlStatementDataRenderer : StatementDataRenderer {
    override fun render(statementData: StatementData): String = createHTMLDocument().html {
        head {
            title {
                +"Statement for ${statementData.customer}"
            }
        }

        body {
            h1 {
                +"Statement for ${statementData.customer}"
            }

            table {
                thead {
                    tr {
                        td { +"Play" }
                        td { +"Amount" }
                        td { +"Seats" }
                    }
                }

                statementData.statementLines.forEach { line ->
                    tr {
                        td { +line.playName }
                        td { +"${asUSD(line.amount)}" }
                        td { +"${line.audience}" }
                    }
                }
            }

            p { +"Amount owed is ${asUSD(statementData.totalAmount)}" }
            p { +"You earned ${statementData.volumeCredits} credits" }
        }
    }.serialize(true)
}

fun asUSD(thisAmount: Int): String? = NumberFormat.getCurrencyInstance(Locale.US).format(thisAmount / 100)