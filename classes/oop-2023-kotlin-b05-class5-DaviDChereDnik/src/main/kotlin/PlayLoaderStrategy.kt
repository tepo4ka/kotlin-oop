import java.io.InputStream

interface PlayLoaderStrategy {
    fun load(stream: InputStream): List<Play>
}

object PlayLoaderFactory {
    fun createPlayLoader(type: String): PlayLoaderStrategy = when (type) {
        "csv" -> PlayCsvLoader
        else -> throw IllegalArgumentException("Unsupported Play loader type $type")
    }
}

object PlayCsvLoader : PlayLoaderStrategy {
    // id,name,type
    // ...
    // id,name,type

    override fun load(stream: InputStream): List<Play> =
        stream.bufferedReader().lineSequence().filter { it.isNotBlank() }.map { line ->
            val (id, name, type) = line.split(',', limit = 3)

            PlayFactory.create(type).createPlay(id, name)
        }.toList()
}
