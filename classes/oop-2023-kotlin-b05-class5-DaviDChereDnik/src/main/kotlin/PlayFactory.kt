interface PlayFactory {
    fun createPlay(id: String, name: String): Play = Play(id, name, calculator)
    val calculator: PlayCalculator

    companion object {
        fun create(type: String): PlayFactory = when (type.lowercase()) {
            "comedy" -> ComedyFactory
            "tragedy" -> TragedyFactory
            else -> throw IllegalArgumentException("Unsupported play type")
        }
    }
}

object ComedyFactory : PlayFactory {
    override val calculator by lazy { ComedyCalculator() }
}

object TragedyFactory : PlayFactory {
    override val calculator by lazy { TragedyCalculator() }
}