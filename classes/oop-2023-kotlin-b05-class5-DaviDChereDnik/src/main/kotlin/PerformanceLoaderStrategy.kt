import java.io.InputStream

interface PerformanceLoaderStrategy {
    fun load(stream: InputStream, plays: List<Play>): List<Performance>
}

object PerformanceLoaderFactory {
    fun createPerformanceLoader(type: String): PerformanceLoaderStrategy = when (type) {
        "csv" -> PerformanceCsvLoader
        else -> throw IllegalArgumentException("Unsupported Performance loader type $type")
    }
}

object PerformanceCsvLoader : PerformanceLoaderStrategy {
    // play-id,audience
    // ...
    // play-id,audience

    override fun load(stream: InputStream, plays: List<Play>): List<Performance> =
        stream.bufferedReader().lineSequence().filter { it.isNotBlank() }.map { line ->
            line.split(',', limit = 2)
        }.mapNotNull { (playId, audience) ->
            val play = plays.firstOrNull { play -> play.id == playId } ?: return@mapNotNull null
            val audienceAmount = audience.toIntOrNull() ?: return@mapNotNull null

            Performance(play, audienceAmount)
        }.toList()
}