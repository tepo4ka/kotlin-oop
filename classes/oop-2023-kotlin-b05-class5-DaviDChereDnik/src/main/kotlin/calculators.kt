import kotlin.math.floor
import kotlin.math.max

interface PlayCalculator {
    fun calculateAmount(perf: Performance): Int

    fun calculateVolumeCredit(perf: Performance): Int = max(perf.audience - 30, 0)
}

class TragedyCalculator : PlayCalculator {
    override fun calculateAmount(perf: Performance): Int {
        var result = 40000
        if (perf.audience > 30) {
            result += 1000 * (perf.audience - 30)
        }
        return result
    }
}

class ComedyCalculator : PlayCalculator {
    override fun calculateAmount(perf: Performance): Int {
        var result = 30000
        if (perf.audience > 20) {
            result += 10000 + 500 * (perf.audience - 20)
        }
        result += 300 * perf.audience
        return result
    }

    override fun calculateVolumeCredit(perf: Performance): Int =
        super.calculateVolumeCredit(perf) + floor(perf.audience / 5.0).toInt()
}

fun volumeCreditsFor(perf: Performance, play: Play): Int = play.calculator.calculateVolumeCredit(perf)

fun amountFor(perf: Performance, play: Play): Int = play.calculator.calculateAmount(perf)