import java.io.File
import java.io.InputStream

class TheaterShell {
    private val plays: MutableList<Play> = mutableListOf()
    private val performances: MutableList<Performance> = mutableListOf()
    private var customer: String = ""

    fun process(command: List<String>) {
        when (command[0]) {
            "load" -> {
                try {
                    when (command[1]) {
                        "plays" -> plays.addAll(
                            PlayLoaderFactory.createPlayLoader(command[2]).load(File(command[3]).inputStream())
                        )

                        "invoice" -> {
                            if (customer != command[3])
                                performances.clear()
                            customer = command[3]

                            performances.addAll(
                                PerformanceLoaderFactory.createPerformanceLoader(command[2])
                                    .load(File(command[4]).inputStream(), plays)
                            )
                        }

                        else -> {
                            println("Unknown load ${command[1]}; ignoring")
                            return
                        }
                    }
                } catch (e: IllegalArgumentException) {
                    println("$e")
                    return
                }
            }

            "add" -> {
                val csvLine = command.subList(2, command.size).joinToString().split(',')
                val addCsvLine = csvLine.first() + ',' + csvLine.subList(1, csvLine.size - 1).joinToString() + ',' + csvLine.last()
                println(csvLine.subList(1, csvLine.size - 1).joinToString ())

                when (command[1]) {
//                    "play" -> plays.addAll(PlayLoaderFactory.createPlayLoader("csv").load(addCsvLine.byteInputStream()))
//                    "performance" -> performances.addAll(
//                        PerformanceLoaderFactory.createPerformanceLoader("csv")
//                            .load(addCsvLine.byteInputStream(), plays)
//                    )

                    else -> {
                        println("Unknown add ${command[1]}; ignoring")
                    }
                }
            }

            "print" -> {
                val (printType, renderer) = command.drop(1)

                when (printType) {
                    "statement" -> println(Statement.create(renderer).render(Invoice(customer, performances)))
                    else -> {
                        println("Unknown print type $printType; ignoring")
                        return
                    }
                }
            }

            else -> {
                println("Unknown command ${command[0]}; ignoring")
                return
            }
        }
    }

    fun run(stream: InputStream) = stream.bufferedReader().lineSequence().forEach { line -> process(line.split(' ')) }

    private fun commandArguments(command: List<String>) = command.drop(1)
}