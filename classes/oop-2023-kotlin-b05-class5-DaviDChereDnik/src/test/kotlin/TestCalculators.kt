import com.appmattus.kotlinfixture.kotlinFixture
import kotlin.math.floor
import kotlin.test.Test
import kotlin.test.assertEquals

class TestCalculators {
    private val fixture = kotlinFixture()

    @Test
    fun testTragedyCalculator() {
        val tragedyCalculator = TragedyCalculator()

        var audience = (1..30).random()
        assertEquals(0, tragedyCalculator.calculateVolumeCredit(Performance(fixture<Play>(), audience)))
        audience = (31..Int.MAX_VALUE).random()
        assertEquals(audience - 30, tragedyCalculator.calculateVolumeCredit(Performance(fixture<Play>(), audience)))

        assertEquals(40000, tragedyCalculator.calculateAmount(Performance(fixture<Play>(), (1..30).random())))
        assertEquals(41000, tragedyCalculator.calculateAmount(Performance(fixture<Play>(), 31)))
        assertEquals(50000, tragedyCalculator.calculateAmount(Performance(fixture<Play>(), 40)))
    }

    @Test
    fun testComedyCalculator() {
        val comedyCalculator = ComedyCalculator()

        var audience = (1..30).random()
        assertEquals(
            floor(audience / 5.0).toInt(),
            comedyCalculator.calculateVolumeCredit(Performance(fixture<Play>(), audience))
        )
        audience = (31..Int.MAX_VALUE).random()
        assertEquals(
            audience - 30 + floor(audience / 5.0).toInt(),
            comedyCalculator.calculateVolumeCredit(Performance(fixture<Play>(), audience))
        )

        assertEquals(30000, comedyCalculator.calculateAmount(Performance(fixture<Play>(), 0)))
        assertEquals(30300, comedyCalculator.calculateAmount(Performance(fixture<Play>(), 1)))
        assertEquals(54000, comedyCalculator.calculateAmount(Performance(fixture<Play>(), 30)))
    }
}