import kotlin.test.Test
import kotlin.test.assertTrue

class TestPlayLoaderStrategy {
    @Test
    fun testCsvLoading() {
        val csvData =
            "hamlet,Hamlet,tragedy\nas-like,As You Like it,comedy\nothello,Othello,tragedy".byteInputStream()

        val csvLoadedData = PlayCsvLoader.load(csvData)
        val csvExpectedData = listOf(
            Play("hamlet", "Hamlet", TragedyCalculator()),
            Play("as-like", "As You Like it", ComedyCalculator()),
            Play("othello", "Othello", TragedyCalculator())
        )

        assertTrue(csvLoadedData.zip(csvExpectedData).all { (loadedPlay, expectedPlay) ->
            loadedPlay.id == expectedPlay.id &&
                    loadedPlay.name == expectedPlay.name &&
                    loadedPlay.calculator::class == expectedPlay.calculator::class
        })

        // check that each calculator is initialized exactly once
        assertTrue(csvLoadedData.groupBy { play -> play.calculator::class }
            .all { (_, plays) -> plays.all { play -> play.calculator === plays.first().calculator } })
    }
}