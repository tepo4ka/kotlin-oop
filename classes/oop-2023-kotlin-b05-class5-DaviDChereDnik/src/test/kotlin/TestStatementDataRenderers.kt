import kotlin.test.Test
import kotlin.test.assertEquals

class TestHtmlStatementDataRenderer {
    @Test
    fun testRenderedOutput() {
        assertEquals(
            "<!DOCTYPE html>\n" +
                    "<html>\n" +
                    "  <head>\n" +
                    "    <META http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n" +
                    "    <title>Statement for BigCo</title>\n" +
                    "  </head>\n" +
                    "  <body>\n" +
                    "    <h1>Statement for BigCo</h1>\n" +
                    "    <table>\n" +
                    "      <thead>\n" +
                    "        <tr>\n" +
                    "          <td>Play</td><td>Amount</td><td>Seats</td>\n" +
                    "        </tr>\n" +
                    "      </thead>\n" +
                    "      <tr>\n" +
                    "        <td>Hamlet</td><td>\$650.00</td><td>55</td>\n" +
                    "      </tr>\n" +
                    "      <tr>\n" +
                    "        <td>As You Like it</td><td>\$580.00</td><td>35</td>\n" +
                    "      </tr>\n" +
                    "      <tr>\n" +
                    "        <td>Othello</td><td>\$500.00</td><td>40</td>\n" +
                    "      </tr>\n" +
                    "    </table>\n" +
                    "    <p>Amount owed is \$1,730.00</p>\n" +
                    "    <p>You earned 47 credits</p>\n" +
                    "  </body>\n" +
                    "</html>\n", Statement.create("html").render(
                Invoice(
                    "BigCo", listOf(
                        Performance(Play("hamlet", "Hamlet", TragedyCalculator()), 55),
                        Performance(Play("as-like", "As You Like it", ComedyCalculator()), 35),
                        Performance(Play("othello", "Othello", TragedyCalculator()), 40)
                    )
                )
            )
        )
    }
}