import kotlin.test.Test
import kotlin.test.assertEquals

class TestStatement {

    @Test
    fun `statement on empty invoice`() {
        val testInvoice = Invoice("testCo", listOf())
        val stmt = """
            Statement for testCo
            Amount owed is $0.00
            You earned 0 credits
            """.trimIndent()
        assertEquals(stmt, Statement.create("text").render(testInvoice).trimIndent())
    }

    @Test
    fun `statement on prod data`() {
        val plays = listOf(
            Play("hamlet", "Hamlet", TragedyCalculator()),
            Play("as-like", "As You Like it", ComedyCalculator()),
            Play("othello", "Othello", TragedyCalculator())
        )
        val invoice = Invoice(
            "BigCo", listOf(
                Performance(plays[0], 55),
                Performance(plays[1], 35),
                Performance(plays[2], 40)
            )
        )
        val stmt = """
            Statement for BigCo
             Hamlet: $650.00 (55 seats)
             As You Like it: $580.00 (35 seats)
             Othello: $500.00 (40 seats)
            Amount owed is $1,730.00
            You earned 47 credits
            """.trimIndent()
        assertEquals(stmt, Statement.create("text").render(invoice).trimIndent())
    }
}