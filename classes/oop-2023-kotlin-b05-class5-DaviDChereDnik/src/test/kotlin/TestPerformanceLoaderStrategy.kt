import kotlin.test.Test
import kotlin.test.assertTrue

class TestPerformanceCsvLoader {
    @Test
    fun testCsvLoading() {
        val plays = listOf(
            Play("hamlet", "Hamlet", TragedyCalculator()),
            Play("as-like", "As You Like it", ComedyCalculator()),
            Play("othello", "Othello", TragedyCalculator())
        )

        val csvData =
            "hamlet,55\nas-like,35\nothello,40\nrepina,30".byteInputStream()

        val csvLoadedPerformances = PerformanceCsvLoader.load(csvData, plays)
        val csvExpectedPerformances = listOf(
            Performance(Play("hamlet", "Hamlet", TragedyCalculator()), 55),
            Performance(Play("as-like", "As You Like it", ComedyCalculator()), 35),
            Performance(Play("othello", "Othello", TragedyCalculator()), 40)
        )

        assertTrue(
            csvLoadedPerformances.map { performance -> performance.play }
                .zip(csvExpectedPerformances.map { performance -> performance.play })
                .all { (loadedPlay, expectedPlay) ->
                    loadedPlay.id == expectedPlay.id &&
                            loadedPlay.name == expectedPlay.name &&
                            loadedPlay.calculator::class == expectedPlay.calculator::class
                })
    }
}