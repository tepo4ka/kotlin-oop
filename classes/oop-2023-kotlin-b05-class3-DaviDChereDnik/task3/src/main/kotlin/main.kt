class Human {
    val greeting = "Привет, я человек!"
}

class Dog {
    val bark = "Гав!"
}

class Alien {
    val command = "Ты меня не видишь"
}

interface Creature {
    val message: String
}

class HumanAdapter(human: Human) : Creature {
    override val message: String = human.greeting
}

class DogAdapter(dog: Dog) : Creature {
    override val message: String = dog.bark
}

class AlienAdapter(alien: Alien) : Creature {
    override val message: String = alien.command
}

class RobotVacuumAdapter(robotVacuum: RobotVacuum) : Creature {
    override val message: String = robotVacuum.sound
}

fun main() {
    val creatures =
        listOf(HumanAdapter(Human()), DogAdapter(Dog()), AlienAdapter(Alien()), RobotVacuumAdapter(RobotVacuum()))

    println("Все сообщения:")
    creatures.forEach { println(it.message) }
}