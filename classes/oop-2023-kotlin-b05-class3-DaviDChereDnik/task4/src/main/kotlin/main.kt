open class Box<T>(private var value: T? = null) {
    fun view(): T = value ?: error("Box is empty")

    fun get(): T = view().also {
        value = null
    }

    fun put(newValue: T) = value?.run { error("Box is not empty") } ?: run { value = newValue }
}

fun <F, T> Box<F>.convert(converter: (F) -> T): Box<T> = Box(converter(this.get()))

class BoxList<T> : ArrayList<Box<T>>() {
    class BoxDecorator<T>(value: T) : Box<T>(value) {
        override fun toString(): String = "[${view()}]"
    }

    @JvmName("BoxList_add")
    fun add(element: T): Boolean = super.add(BoxDecorator(element))
}

fun part1() {
    println("=== Часть 1: класс Box ===")
    val b = Box(6)
    println("Box b: ${b.view()}")
    try {
        b.put(6)
    } catch (e: IllegalStateException) {
        println("ERROR: ${e.message}")
    }
    b.get()
    try {
        println("Box b: ${b.view()}")
    } catch (e: IllegalStateException) {
        println("ERROR: ${e.message}")
    }
    try {
        b.get()
    } catch (e: IllegalStateException) {
        println("ERROR: ${e.message}")
    }
    b.put(0)
    println("Box b: ${b.view()}")

    val b2 = Box("hello")
    println("Box b2: ${b2.view()}")
    val s = b2.get()
    println("String's length is ${s.length}")
    b2.put("bye")
    val s2 = b2.get()
    println("String's length is ${s2.length}")
}

fun part2() {
    println("\n=== Часть 2: функция convert ===")
    val intBox = Box(42)
    val stringBox = intBox.convert { "$it!" }
    println("Box stringBox: ${stringBox.view()}")
    try {
        println("Box intBox: ${intBox.view()}")
    } catch (e: IllegalStateException) {
        println("ERROR: ${e.message}")
    }
    val intBox2 = stringBox.convert { it.length }
    println("Box intBox2: ${intBox2.view()}")
    val intBox3 = intBox2.convert { it + 1 }
    println("Box intBox3: ${intBox3.view()}")
}

fun part3() {
    println("\n=== Часть 3: класс BoxList ===")
    val intBoxes = BoxList<Int>()
    (1..8).forEach { intBoxes.add(it) }
    println(intBoxes)

    val stringBoxes = BoxList<String>()
    ('a'..'h').map { it.toString() }
        .forEach { stringBoxes.add(it) }
    println(stringBoxes)
}

fun main() {
    part1()
    part2()
    part3()
}