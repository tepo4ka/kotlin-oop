interface Talkable {
    val sound: String

    fun talk()
}

fun String.repeat(n: Int, separator: String): String = List(n) { this }.joinToString(separator = separator)

abstract class AbstractAnimal : Talkable {
    override fun talk() = println(sound.repeat(3, "-"))
}

class Cat : AbstractAnimal() {
    override val sound: String = "мяу"
}

class Dog : AbstractAnimal() {
    override val sound: String = "гав"
}

class Goose : AbstractAnimal() {
    override val sound: String = "га"
}

class RobotVacuum : Talkable {
    override val sound: String = "ур"

    override fun talk() = println(sound.repeat(4, "-"))
}

fun main() {
    val animals = listOf(Cat(), Dog(), Goose(), RobotVacuum())
    animals.forEach { it.talk() }
}