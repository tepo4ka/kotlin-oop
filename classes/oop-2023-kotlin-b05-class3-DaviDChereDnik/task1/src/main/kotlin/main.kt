interface Computer {
    fun calculateAnswer(): Int
}

class Desktop(val answer: Int) : Computer {
    override fun calculateAnswer(): Int = answer
}

class SummingCloud(val nodesAmount: Int) : Computer {
    private val nodes: List<Computer> = buildList {
        repeat(nodesAmount) { n ->
            add(Desktop(n + 1))
        }
    }

    override fun calculateAnswer(): Int = nodes.sumOf { computer -> computer.calculateAnswer() }
}

fun main() {
    val answer = SummingCloud(10).calculateAnswer()
    println("Sum = $answer") // should print 55
}