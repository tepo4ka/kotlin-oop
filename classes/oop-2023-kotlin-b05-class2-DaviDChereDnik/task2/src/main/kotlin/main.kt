abstract class Animal {
    abstract val animalName: String
    abstract val animalDoes: String
    abstract val animalVocalization: String

    fun talk() = println("${animalName.replaceFirstChar { it.titlecase() }} $animalDoes '$animalVocalization'")
}

class Cat : Animal() {
    override val animalName: String = "кошка"
    override val animalDoes: String = "мяучит"
    override val animalVocalization: String = "мяу-мяу"
}

class Dog : Animal() {
    override val animalName: String = "собака"
    override val animalDoes: String = "гавкает"
    override val animalVocalization: String = "гав-гав-гав"
}

class Goose : Animal() {
    override val animalName: String = "гусь"
    override val animalDoes: String = "гогочет"
    override val animalVocalization: String = "га-га-га"
}

fun runTest() {
    val animals = listOf(Cat(), Dog(), Goose())
    animals.forEach { it.talk() }
}

fun main() {
    runTest()
}