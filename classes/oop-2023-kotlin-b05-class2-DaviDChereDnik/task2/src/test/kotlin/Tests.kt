import java.io.ByteArrayOutputStream
import java.io.PrintStream
import kotlin.reflect.full.declaredFunctions
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

internal class Task2Tests {
    private val standardOut = System.out
    private val stream = ByteArrayOutputStream()

    @BeforeTest
    fun setUp() {
        System.setOut(PrintStream(stream, true, Charsets.UTF_8))
    }

    @AfterTest
    fun tearDown() {
        System.setOut(standardOut)
    }

    @Test
    fun `check runTest output`() {
        runTest()
        assertEquals(
            """
            Кошка мяучит 'мяу-мяу'
            Собака гавкает 'гав-гав-гав'
            Гусь гогочет 'га-га-га'
        """.trimIndent(), stream.toString(Charsets.UTF_8).trim().lines().joinToString("\n")
        )
    }

    @Test
    fun `Animal is abstract`() {
        ClassLoader.getSystemClassLoader().loadClass("Animal").kotlin.apply {
            assertTrue(isAbstract)
        }
    }


    @Test
    fun `no overriding 'talk' in classes`() {
        assertNoFunctionsDeclared("Cat")
        assertNoFunctionsDeclared("Dog")
        assertNoFunctionsDeclared("Goose")
    }

    private fun assertNoFunctionsDeclared(className: String) {
        ClassLoader.getSystemClassLoader().loadClass(className).kotlin.apply {
            assertTrue(declaredFunctions.isEmpty(), "Class $className is expected to have no functions declared")
        }
    }

}