fun runTest() {
    val ageProperty = JsonIntProperty("age", 21)
    println(ageProperty)
    println(ageProperty.stringRepresentation)
    ageProperty.value += 1
    println(ageProperty)
    ageProperty.stringRepresentation = "age: 23"
    println(ageProperty)
    ageProperty.stringRepresentation = "age   :   24"
    println(ageProperty)
    try {
        ageProperty.stringRepresentation = "value : 10"
    } catch(e: Exception) {
        println(e)
    }
    try {
        ageProperty.stringRepresentation = "age: ?"
    } catch(e: Exception) {
        println(e)
    }
    try {
        ageProperty.stringRepresentation = "age = 10"
    } catch(e: Exception) {
        println(e)
    }
    println("JSON value of 'age' has been set ${ageProperty.setValueCounter} time(s)")
    val countProperty = JsonIntProperty("count")
    println(countProperty)
    println("JSON value of 'count' has been set ${countProperty.setValueCounter} time(s)")
    println("Class 'JsonIntProperty' instance has been created ${JsonIntProperty.instanceCounter} time(s)")
}

fun main() {
    runTest()
}