class JsonIntProperty(val key: String, value: Int = 0) {
    var setValueCounter = 1

    var value = value
        set(newValue) {
            if (newValue != field) {
                field = newValue
                setValueCounter++
            }
        }

    var stringRepresentation
        get() = "$key: $value"
        set(newString) {
            val (parsedKey, parsedValue) = parseJsonPropertyString(newString)
            require(parsedKey == key) { "Property name is immutable" }

            value = parsedValue
        }

    companion object {
        var instanceCounter = 0

        private val propertyRegex = """(\w+)\s*:\s*(\S+)""".toRegex()

        private fun parseJsonPropertyString(s: String): Pair<String, Int> {
            val parsedString = propertyRegex.matchEntire(s)
            require(parsedString != null && parsedString.groupValues.size == 3) { "Incorrect JSON property format: '$s'" }

            return parsedString.groupValues[1] to parsedString.groupValues[2].toInt()
        }
    }

    init {
        instanceCounter++
    }

    override fun toString(): String = stringRepresentation
}