import java.io.ByteArrayOutputStream
import java.io.PrintStream
import kotlin.test.*

internal class Tests {
    private val standardOut = System.out
    private val stream = ByteArrayOutputStream()

    @BeforeTest
    fun setUp() {
        System.setOut(PrintStream(stream))
    }

    @AfterTest
    fun tearDown() {
        System.setOut(standardOut)
    }

    @Test
    fun `check runTest output` () {
        runTest()
        assertEquals("""
            age: 21
            age: 21
            age: 22
            age: 23
            age: 24
            java.lang.IllegalArgumentException: Property name is immutable
            java.lang.NumberFormatException: For input string: "?"
            java.lang.IllegalArgumentException: Incorrect JSON property format: 'age = 10'
            JSON value of 'age' has been set 4 time(s)
            count: 0
            JSON value of 'count' has been set 1 time(s)
            Class 'JsonIntProperty' instance has been created 2 time(s)
        """.trimIndent(), stream.toString().trim().lines().joinToString("\n"))
    }
}