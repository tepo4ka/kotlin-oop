abstract class Creature

class Human : Creature() {
    fun greeting() = "Привет, я человек!"
}

class Dog : Creature() {
    fun bark() = "Гав!"
}

class Alien : Creature() {
    fun command() = "Ты меня не видишь"
}

fun printMessageFrom(creature: Creature) = println(
    when (creature) {
        is Human -> creature.greeting()
        is Dog -> creature.bark()
        is Alien -> creature.command()
        else -> "Незнакомое существо что-то сообщает"
    }
)

fun findDogs(creatures: List<Creature>): List<Dog> = creatures.filterIsInstance<Dog>()

fun runTest() {
    val creatures = listOf(Alien(), Dog(), Human(), Dog())
    println("Все сообщения:")
    creatures.forEach { printMessageFrom(it) }
    println("\nСообщения только от собак:")
    findDogs(creatures).forEach { println(it.bark()) }
}

fun main() {
    runTest()
}