import java.io.ByteArrayOutputStream
import java.io.PrintStream
import kotlin.test.*

internal class Task3Tests {
    private val standardOut = System.out
    private val stream = ByteArrayOutputStream()

    @BeforeTest
    fun setUp() {
        System.setOut(PrintStream(stream, true, Charsets.UTF_8))
    }

    @AfterTest
    fun tearDown() {
        System.setOut(standardOut)
    }

    @Test
    fun `check runTest output` () {
        runTest()
        assertEquals("""
            Все сообщения:
            Ты меня не видишь
            Гав!
            Привет, я человек!
            Гав!

            Сообщения только от собак:
            Гав!
            Гав!
        """.trimIndent(), stream.toString(Charsets.UTF_8).trim().lines().joinToString("\n"))
    }

    @Test
    fun `function 'printMessageFrom' works for dogs`() {
        Class.forName("MainKt")
            .declaredMethods
            .find { it.name == "printMessageFrom" }
            ?.invoke(null, Dog())
        assertEquals(Dog().bark(), stream.toString(Charsets.UTF_8).trim())
    }

    @Test
    fun `function 'printMessageFrom' works for humans`() {
        Class.forName("MainKt")
            .declaredMethods
            .find { it.name == "printMessageFrom" }
            ?.invoke(null, Human())
        assertEquals(Human().greeting(), stream.toString(Charsets.UTF_8).trim())
    }

    @Test
    fun `function 'printMessageFrom' works for aliens`() {
        Class.forName("MainKt")
            .declaredMethods
            .find { it.name == "printMessageFrom" }
            ?.invoke(null, Alien())
        assertEquals(Alien().command(), stream.toString(Charsets.UTF_8).trim())
    }

    @Test
    fun `function 'findDogs' is correct`() {
        val findDogsMethod = Class.forName("MainKt")
            .declaredMethods
            .find { it.name == "findDogs" }
        
        val dogs1 = findDogsMethod
            ?.invoke(null, listOf(Alien(), Dog(), Human(), Dog())) as? List<Dog>
        assertEquals(2, dogs1?.size)

        val dogs2 = findDogsMethod
            ?.invoke(null, listOf(Alien(), Human(), Dog())) as? List<Dog>
        assertEquals(1, dogs2?.size)
    }
}
