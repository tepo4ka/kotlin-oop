import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNotEquals
import kotlin.test.assertTrue

internal class Task4Tests {
    private val randomRepeat = 30

    @Test
    fun `single-use sensors`() {
        val sys = SensorSystem(1, 1)
        repeat(randomRepeat) {
            val curSensor = sys.getSensor(getRandomSensorType())
            curSensor.value

            assertEquals(sys.totalSensorCount, sys.usageCount)
        }
    }

    @Test
    fun `at least one measurement is available`() {
        repeat(randomRepeat) {
            val sys = getRandomSensorSystem()
            val sensor = sys.getSensor(getRandomSensorType())

            assertTrue { sensor.active }
        }

        repeat(randomRepeat) {
            val maxRequestPrice = (1..1000).random()
            assertFailsWith<IllegalArgumentException> { SensorSystem((1 until maxRequestPrice).random(), maxRequestPrice) }
        }
    }

    @Test
    fun `no premature sensor creation`() {
        repeat(randomRepeat) {
            val sys = getRandomSensorSystem()

            val sensors = buildList {
                SensorSystem.SensorType.values().forEach { type ->
                    add(sys.getSensor(type))
                }
            }

            sensors.forEach { sensor ->
                do {
                    sensor.value
                } while (sensor.active)
            }

            assertEquals(0, sys.currentSensorCount)
            assertEquals(SensorSystem.SensorType.values().size, sys.totalSensorCount)
        }
    }

    @Test
    fun `no sensors in fresh system`() {
        repeat(randomRepeat) {
            assertEquals(0, getRandomSensorSystem().currentSensorCount)
        }
    }

    @Test
    fun `there is at most one sensor of its type`() {
        repeat(randomRepeat) {
            val sys = getRandomSensorSystem()
            val curSensorType = getRandomSensorType()
            val curSensor = sys.getSensor(curSensorType)

            assertEquals(curSensor, sys.getSensor(curSensorType))

            curSensor.value
            assertEquals(curSensor, sys.getSensor(curSensorType))
        }
    }

    @Test
    fun `non-active sensor is not stored`() {
        repeat(randomRepeat) {
            val sys = getRandomSensorSystem()
            val sensorType = getRandomSensorType()
            val sensor = sys.getSensor(sensorType)

            assertEquals(sensor, sys.getSensor(sensorType))

            while (sensor.active) sensor.value

            assertNotEquals(sensor, sys.getSensor(sensorType))
        }
    }

    private fun getRandomSensorSystem(): SensorSystem {
        val initialSensorResource = (1..1000).random()
        return SensorSystem(initialSensorResource, (1..initialSensorResource).random())
    }

    private fun getRandomSensorType(): SensorSystem.SensorType = SensorSystem.SensorType.values().random()
}