import kotlin.random.Random

fun main() {
    val sys = SensorSystem(5, 2)
    for(i in 1 .. 5) {
        val type = SensorSystem.SensorType.values().random()
        val sensor = sys.getSensor(type)
        do {
            println("Value ($type): ${sensor.value}")
        } while(sensor.active && Random.nextInt(2) == 1)
    }
    println("Current sensor count: ${sys.currentSensorCount}")
    println("Total sensor count: ${sys.totalSensorCount}")
    println("Total usage count: ${sys.usageCount}")
}