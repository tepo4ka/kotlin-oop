import kotlin.random.Random

class SensorSystem(private val initialSensorResource: Int, private val maxRequestPrice: Int) {
    val currentSensorCount: Int
        get() = sensors.size
    var totalSensorCount: Int = 0
        private set
    var usageCount: Int = 0
        private set

    private val sensors: MutableMap<SensorType, Sensor> = mutableMapOf()

    init {
        require(initialSensorResource >= maxRequestPrice) { "Acquired sensors has to be able to do at least 1 request" }
    }

    fun getSensor(type: SensorType): Sensor = sensors.getOrPut(type) {
        totalSensorCount++

        return@getOrPut Sensor()
    }

    enum class SensorType {
        TYPE_A, TYPE_B, TYPE_C
    }

    inner class Sensor {
        private var resource = initialSensorResource

        val active: Boolean
            get() = resource > 0

        val value: Double
            get() {
                check(active) { "Non-active sensors can't be requested" }

                resource -= (1..maxRequestPrice).random()

                if (!active)
                    this@SensorSystem.sensors.values.remove(this)

                usageCount++

                return Random.nextDouble()
            }
    }
}